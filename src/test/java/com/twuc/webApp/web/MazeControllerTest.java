package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest
@AutoConfigureMockMvc
class MazeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_is_ok_when_call_api() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/mazes/color"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_is_400_when_call_api_with_error_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/mazes/colo"))
                .andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").exists());
    }
}