package com.twuc.webApp.domain.mazeRender.areaRenders;

import com.twuc.webApp.domain.mazeRender.AreaRender;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * 使用图片平铺渲染一整块区域。
 */
public class AreaTileImageRender implements AreaRender {

    private final BufferedImage tile;

    /**
     * 创建一个 {@link AreaTileImageRender} 实例。
     *
     * @param tile 作为平铺背景的图像。
     */
    public AreaTileImageRender(BufferedImage tile) {
        this.tile = tile;
    }

    @Override
    public void render(Graphics2D graphics, Rectangle fullArea) {
        graphics.setPaint(new TexturePaint(tile, new Rectangle(0, 0, tile.getWidth(), tile.getHeight())));
        graphics.fillRect(fullArea.x, fullArea.y, fullArea.width, fullArea.height);
    }
}
